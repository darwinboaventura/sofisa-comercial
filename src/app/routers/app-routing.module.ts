import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '@sofisa/core';

const routes: Routes = [
	{
		path: '',
		redirectTo: 'dashboard',
		pathMatch: 'full'
	},
	{
		path: 'login',
		loadChildren: '../components/login/login.module#LoginModule'
	},
	{
		path: 'dashboard',
		canActivate: [AuthGuard],
		loadChildren: '../components/dashboard/dashboard.module#DashboardModule'
	},
	{
		path: 'tasklist',
		canActivate: [AuthGuard],
		loadChildren: '../components/tasklist/tasklist.module#TasklistModule'
	},
	{
		path: 'styleguide',
		loadChildren: '../components/styleguide/styleguide.module#StyleguideModule'
	}
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})

export class AppRoutingModule {}
