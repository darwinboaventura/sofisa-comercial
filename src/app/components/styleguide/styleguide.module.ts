import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {StyleguideComponent} from './styleguide.component';
import {StyleguideRoutingModule} from './styleguide-routing.module';
import {ButtonModule, InputModule, TextareaModule, CheckboxModule, RadioboxModule, SwitchModule} from '@sofisa/ui';

/**
	É responsável pela página `styleguide`.

	Importa todos os módulos do `@sofisa/ui`.
	
	Ele carrega o módulo de rota `StyleguideRoutingModule`. Esse é responsável por especificar as rotas e qual componente carregar ao acessa-lás. 
*/

@NgModule({
	declarations: [StyleguideComponent],
	imports: [
		CommonModule,
		ButtonModule,
		InputModule,
		TextareaModule,
		CheckboxModule,
		RadioboxModule,
		SwitchModule,
		StyleguideRoutingModule
	],
	exports: [StyleguideComponent]
})

export class StyleguideModule {}
