import { NgModule } from '@angular/core';
import { MainModule } from '@sofisa/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';

/**
	Módulo responsável pela página `Dashboard`

	Ele carrega o módulo de rota `DashboardRoutingModule`. Esse é responsável por especificar as rotas e qual componente carregar ao acessa-lás. 

	Atualmente possui apenas HTML estatico.
*/

@NgModule({
	declarations: [DashboardComponent],
	imports: [
		CommonModule,
		DashboardRoutingModule,
		MainModule
	],
	exports: [DashboardComponent]
})

export class DashboardModule {}
