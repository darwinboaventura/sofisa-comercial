import {AuthService} from '@sofisa/core';
import {Component, OnInit} from '@angular/core';

/**
	Utiliza o component `LoginForm` para exibir o formulário de login.

	Possui o metódo `handleLogin`, esse receberá os valores preenchidos no formulário e deverá fazer a autenticação utilizando o metódo `doLogin` do serviço `AuthService`.
*/

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {
	constructor(public authService: AuthService) {}
	
	ngOnInit() {}
	
	handleLogin(values: any) {		
		if (values) {
			this.authService.doLogin(values);
		}
	}
}
