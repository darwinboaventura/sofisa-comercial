import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from '@sofisa/core';
import { LoaderService } from '@sofisa/loader';
import { NotificationService } from '@sofisa/notification';

/**
	É o primeiro component a ser carregado.

	Nele é carregado a estrutura básica da aplicação (header, nav, loader, notification). Toda rota será carregada dentro da tag `router-outlet`.
*/

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})

export class AppComponent {
	public notification$: Observable<any>;

	constructor(public authService: AuthService, public loaderService: LoaderService, public notificationService: NotificationService) {
		this.authService.init();
	}
}
