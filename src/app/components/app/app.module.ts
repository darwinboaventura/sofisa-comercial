import {NgModule} from '@angular/core';
import {OAuthModule} from 'angular-oauth2-oidc';
import {StoreModule} from '@ngrx/store';
import {AppComponent} from './app.component';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from '../../routers/app-routing.module';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {LoaderModule, LoaderService, loaderReducer} from '@sofisa/loader';
import {AuthService, AuthInterceptor, HeaderModule, AsideModule} from '@sofisa/core';
import {NotificationModule, NotificationService, notificationReducer} from '@sofisa/notification';

/**
	Module principal, responsável por bootstrap e por iniciar os serviços que deverão está acessiveis para toda aplicação.

	ngrx: 
	
	- Deve ser importado através do `StoreModule.forRoot({})` apenas os reducers que atendem toda a aplicação. 
	- Reducers que serão utilizado apenas uma vez deverão ser importados através do `StoreModule.forFeature({})`
*/

@NgModule({
	declarations: [AppComponent],
	imports: [
		BrowserModule,
		AppRoutingModule,
		HttpClientModule,
		HeaderModule,
		AsideModule,
		LoaderModule,
		NotificationModule,
		StoreModule.forRoot({
			notification: notificationReducer,
			loader: loaderReducer
		}),
		OAuthModule.forRoot()
	],
	providers: [
		AuthService,
		{
			provide: HTTP_INTERCEPTORS,
			useClass: AuthInterceptor,
			multi: true
		},
		LoaderService,
		NotificationService
	],
	bootstrap: [AppComponent],
	exports: []
})

export class AppModule {}
