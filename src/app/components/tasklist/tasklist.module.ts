import {NgModule} from '@angular/core';
import {StoreModule} from '@ngrx/store';
import {ButtonModule} from '@sofisa/ui';
import {CommonModule} from '@angular/common';
import {TasklistComponent} from './tasklist.component';
import {TasklistRoutingModule} from './tasklist-routing.module';
import {MainModule, taskReducer} from '@sofisa/core';
import {TasklistModule as TasklistModuleBpm} from '@sofisa/bpm';

/**
	Module principal do BPM.

	Módulos importados:

	- MainModule
	- ButtonModule,
	- TasklistModuleBpm

	RoutingModule: 
	
	- TasklistRoutingModule

	Reducers:

	- Task
*/

@NgModule({
	declarations: [TasklistComponent],
	imports: [
		CommonModule,
		MainModule,
		ButtonModule,
		TasklistModuleBpm,
		TasklistRoutingModule,
		StoreModule.forFeature('pages', {
			pages: taskReducer
		})
	],
	exports: [TasklistComponent]
})

export class TasklistModule {}
