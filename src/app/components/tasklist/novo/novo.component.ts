import {Store} from '@ngrx/store';
import {Router} from '@angular/router';
import {Component } from '@angular/core';
import {BpmService} from '@sofisa/bpm';
import {Observable} from 'rxjs';
import {handleError} from '@sofisa/core';
import {LoaderService} from '@sofisa/loader';
import {NotificationService} from '@sofisa/notification';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

/**
	Carrega o component `TaskForm` e o alimenta através da propriedade `props`. Recebe os valores preenchidos através do evento `submission`.
*/

@Component({
	selector: 'app-novo',
	templateUrl: './novo.component.html',
	styleUrls: ['./novo.component.scss']
})

export class NovoComponent {
	pages$: Observable<any>;

	state: any = {
		page: 'tasklist/novo',
		form: {
			formGroup: this.fb.group({
				variables: this.fb.group({
					cliente: this.fb.group({
						value: [null, Validators.required],
						type: ['String'],
						valueInfo: [{}]
					}),
					cnpj: this.fb.group({
						value: [null, Validators.required],
						type: ['String'],
						valueInfo: [{}]
					}),
					dataVisita: this.fb.group({
						value: [null, Validators.required],
						type: ['String'],
						valueInfo: [{}]
					})
				})
			}),
			validations: {
				cliente: {
					type: '',
					message: ''
				},
				cnpj: {
					type: '',
					message: ''
				},
				dataVisita: {
					type: '',
					message: ''
				}
			},
			actions: {
				submitted: false
			}
		}
	};
	
	constructor(public fb: FormBuilder, public bpmService: BpmService, public router: Router, public notificationService: NotificationService, public store: Store<{ pages: any, loader: any }>, public loaderService: LoaderService) {}
	
	/**
	 * Recebe os valores preenchidos no formulário, verifica se são válidos.
	 *  
	 * Caso seja válido:
	 * - Carrega o loader
	 * - Utiliza o método `startABpm` do serviço `BpmService` passando os valores do formulário.	 * 
	 * - Após finalizar requisição será removido o loader e exibido a mensagem de sucesso.
	 * 
	 * @param {FormGroup} formGroup Recebe formulário instancia do component TaskForm
	 */
	handleTaskCreation(formGroup: FormGroup) {
		if (formGroup.valid) {
			this.loaderService.addAnItemToLoader({
				page: 'tasklist/novo',
				name: 'start'
			});
			
			this.bpmService.startABpm(formGroup.value).subscribe(
				(res: any) => {
					this.loaderService.removeAnItemToLoader({
						page: 'tasklist/novo',
						name: 'start'
					});

					this.notificationService.addMessage({
						type: 'success',
						message: 'A tarefa foi criada com sucesso!'
					});
					
					this.router.navigateByUrl('tasklist');
				},
				(err) => {
					handleError.call(this, err, { page: 'tasklist/novo', name: 'start'});
				}
			);
		}
	}
}


