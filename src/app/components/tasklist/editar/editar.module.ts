import {NgModule} from '@angular/core';
import {MainModule} from '@sofisa/core';
import {CommonModule} from '@angular/common';
import {EditarComponent} from './editar.component';
import {EditarRoutingModule} from './editar-routing.module';
import {ReactiveFormsModule} from '@angular/forms';
import {TaskFormModule, DrawingModule} from '@sofisa/bpm';

/**
	Module responsável pela página `Editar uma task`.

	Módulos importados:

	- MainModule
	- TaskFormModule
	- DrawingModule

	RoutingModule: 
	
	- EditarRoutingModule
*/

@NgModule({
	declarations: [EditarComponent],
	imports: [
		CommonModule,
		MainModule,
		TaskFormModule,
		DrawingModule,
		ReactiveFormsModule,
		EditarRoutingModule
	],
	exports: [EditarComponent]
})

export class EditarModule {}
